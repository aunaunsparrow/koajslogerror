const service = require('../services/ErrorService')

module.exports = {

    GetAllError: async (ctx, next) => {
        var getAllError = await service.GetAllError()
        ctx.body = getAllError
        
    },
}