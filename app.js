const Koa = require('koa')
const bodyParser = require("koa-bodyparser")
const app = new Koa();
const router = require('./routes/routes')
const cors = require('@koa/cors')
const job = require('./corehelper/cronJob/jobDeletLogInDatabase')
const WinstonLog = require('./winstonLog/winstonsLog')
var model = require('./viewModel/testViewModel')
var logErrorModel = require('./viewModel/LogErrorViewModel')
// body parser
app.use(bodyParser());
app.use(
    cors({
        origin:'*',
        allowMethods:['GET','HEAD','PUT','POST','DELETE','PATCH'],
        exposeHeaders:['X Request id'],
    })
)
//test OOP
/* can't list of instance!!
var testObj = new model.Student()
var testObj1 = new model.ListRoom()
testObj1.room = 'ป.6'
testObj1.subRoom = 1
let pp = []
var t= [0,1,2,3,4,5,6,7,8,9]
for(let x in t){

testObj.id = x
testObj.age = x
testObj.name = `name ${x}`
testObj1.ListRoomObject(testObj)
}
console.log(testObj1)
//console.log(testObj1.listStudent[0])
/*var i
for(i=0;i<10;i++){
test._list.push(test._age+`${i}`)

}*/

app.use(async (ctx, next) => {
    try {
      await next();
    } catch (err) {
      var datetime = new Date().toLocaleString({timeZone:"Asia/Bangkok"}) 
      var errorModel = new logErrorModel.LogError(err.status || 500,err.message,datetime)
       
      await  WinstonLog.LogError(errorModel)
      var resModel = new logErrorModel.LogError(errorModel.status,errorModel.message,errorModel.datetime)
        ctx.status = resModel.status
        ctx.body = resModel
        ctx.app.emit('error', err, ctx);
    }
  })
  //cronJob ยังไม่ได้เซตเวลา 1 clear/sec
/*job.JobDeletLogError( ()=>{
    console.log('Start Job')
})*/
app.use(router.routes())
// server
app.listen(3000);
