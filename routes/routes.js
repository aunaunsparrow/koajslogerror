const Router =require('koa-router')
const router =new Router()
const test=require('../controllers/test');
const errorController=require('../controllers/ErrorController');

router.post('/test',test.TestWinston)
router.get('/test/GetAllError',errorController.GetAllError)

module.exports = router
