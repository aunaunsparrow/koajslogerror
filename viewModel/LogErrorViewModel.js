class LogError {
    constructor(statusError, errorMessage, timeError) {
        this.status = statusError
        this.message = errorMessage
        this.datetime = timeError
    }
}

class GetAllLogError{
    constructor(project,object){
        this.project = project
        this.errors = object
        
    }
}
module.exports = {LogError,GetAllLogError}