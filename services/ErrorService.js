const db = require('../config/mySql')
const errorModel = require('../viewModel/LogErrorViewModel')

module.exports={

    async GetAllError()
    {
        var result_token =await (await db).query("SELECT * FROM `errors`")
        var distinct = [...new Set(result_token.map(x=>x.Project))].sort()
        let list = new Array()

        for(let p of distinct){
            var model = new errorModel.GetAllLogError()
            model.project = p
            model.errors = result_token.filter(q=>q.Project == p)
            list.push(model)
        }
        
        return list
    },
}