const mySql = require('promise-mysql')
const configMysql = {
    host: "localhost",
    port: "3306",
    user: "root",
    password: "",
    database: "logerror",
    connectionLimit: 100
};
const connectMysql = mySql.createPool(configMysql)
module.exports = connectMysql